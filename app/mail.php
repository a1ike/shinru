<?php
header('Content-Type: application/json');
# error_reporting(E_ALL);
# ini_set("display_errors", 1);

$path_mailer_class = pathinfo(__FILE__);
require_once($path_mailer_class['dirname'].'/phpmailer/PHPMailerAutoload.php');


function add_data($text, $data) {
	if(isset($data) and strlen($data)) {
		return $text.': '.$data.'<br>';
	}
	return '';
}


$our_order_id = round(microtime(true) * 1000);

$mail = new PHPMailer;

# $mail->SMTPDebug = 2;
# $mail->XMailer = 'The bat! v2.99';
$mail->setLanguage('ru', '.');
$mail->isSMTP();
$mail->Host = 'smtp.yandex.ru';
$mail->SMTPAuth = true;
$mail->Username = 'roses.4ever@yandex.ru';
$mail->Password = '1kf9e6hn';
$mail->SMTPSecure = 'ssl';
$mail->Port = 465;
$mail->CharSet = 'UTF-8';

$mail->setFrom('s-qviz@yandex.ru', 'Шип.ру | Онлайн-магазин автомобильных шин и дисков в Москве');
/* $mail->addAddress('it@gmail.com', 'Шип.ру | Онлайн-магазин автомобильных шин и дисков в Москве'); */

/*
# Для отправки копии письма
if(isset($input_data['post']['test'])) {
	$mail->AddCC('it@gmail.com', 'Мне');
}
*/

$mail->isHTML(true);

$mail->Subject = 'Новая заявка на сайте Шип.ру = '.$our_order_id;
$mail->Body = '<html><body>';
$mail->Body .= add_data('ID Заявки', $our_order_id);
$mail->Body .= add_data('Сезонность шин', $_POST['data_type']);
$mail->Body .= add_data('Тип дисков', $_POST['data_disk']);
$mail->Body .= add_data('Категория цен', $_POST['data_category']);
$mail->Body .= add_data('Типоразмер', $_POST['data_size']);
$mail->Body .= add_data('Марка и и модель машины', $_POST['data_model']);
$mail->Body .= add_data('Имя', $_POST['data_person']);
$mail->Body .= add_data('E-mail', $_POST['data_email']);
$mail->Body .= add_data('Телефон', $_POST['data_phone']);
$mail->Body .= add_data('Способ получения', $_POST['data_get']);


$mail->Body .= add_data('=================', 'Техническия информация:');
$mail->Body .= add_data('Страница заказа', $_SERVER['HTTP_REFERER']);
$mail->Body .= add_data('IP клиента', $_SERVER['REMOTE_ADDR']);
$mail->Body .= add_data('UserAgent клиента', $_SERVER['HTTP_USER_AGENT']);

$mail->Body .= '</body></html>';

# Если письмо отправилось, следовательно заказ создан
if($mail->send()) {
	print(json_encode([
		'response' => 'ok',
		'order_id' => $our_order_id,
	]));
}
else {
	print(json_encode([
		'response' => 'fail',
	]));
}
