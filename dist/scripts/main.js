'use strict';

$(document).ready(function () {

  $('#open_1_1').on('click', function () {
    $('.s-home').slideToggle('fast');
    $('.s-steps').slideToggle('fast');
    $('#step_1_1').slideToggle('fast');
  });

  $('#open_1_2').on('click', function () {
    $('.s-home').slideToggle('fast');
    $('.s-steps').slideToggle('fast');
    $('#step_1_2').slideToggle('fast');
  });

  $('#open_2_1').on('click', function () {
    $('#step_1_1').slideToggle('fast');
    $('#step_2').slideToggle('fast');
    $('#gift_2').removeClass('s-gifts__card_locked');
    $('#gift_2').addClass('s-gifts__card_animation');
  });

  $('#open_2_2').on('click', function () {
    $('#step_1_2').slideToggle('fast');
    $('#step_2').slideToggle('fast');
    $('#gift_2').removeClass('s-gifts__card_locked');
    $('#gift_2').addClass('s-gifts__card_animation');
  });

  $('#open_3').on('click', function () {
    $('#step_2').slideToggle('fast');
    $('#step_3').slideToggle('fast');
    $('#gift_3').removeClass('s-gifts__card_locked');
    $('#gift_3').addClass('s-gifts__card_animation');
  });

  $('#open_4').on('click', function () {

    $('#step_3').slideToggle('fast');
    $('.s-steps').slideToggle('fast');

    $('.s-loading').slideToggle('fast', function () {
      var elem = document.getElementById('ui_progress');
      var percent = document.getElementById('ui_loading');
      var width = 69;
      var id = setInterval(frame, 75);

      function frame() {
        if (width >= 100) {
          clearInterval(id);
          $('.s-loading').slideToggle('fast');
          $('.s-steps').slideToggle('fast');
          $('#step_4').slideToggle('fast');
        } else {
          width++;
          elem.style.width = width + '%';
          percent.innerHTML = width;
        }
      }
    });

    $('#gift_4').removeClass('s-gifts__card_locked');
    $('#gift_4').addClass('s-gifts__card_animation');
  });

  $('.phone').inputmask({
    mask: '+7(999)999-99-99',
    showMaskOnHover: false,
    showMaskOnFocus: true
  });

  $('.s-inputs__input input').focusout(function () {
    if ($(this).val()) {
      $(this).parent().addClass('s-inputs__input_complete');
    } else {
      $(this).parent().removeClass('s-inputs__input_complete');
    }
  });

  $('.s-final__input input').focusout(function () {
    if ($(this).val()) {
      $(this).parent().addClass('s-final__input_complete');
    } else {
      $(this).parent().removeClass('s-final__input_complete');
    }
  });

  $('.s-modal__input input').focusout(function () {
    if ($(this).val()) {
      $(this).parent().addClass('s-modal__input_complete');
    } else {
      $(this).parent().removeClass('s-modal__input_complete');
    }
  });

  $('.open-modal').on('click', function (e) {
    e.preventDefault();
    $('.s-modal').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.s-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.s-modal').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('input[name="data_modal_phone"]').on('input', function () {
    if ($(this).inputmask('isComplete')) {
      $('.s-modal__button').removeClass('unclickable');
    } else {
      $('.s-modal__button').addClass('unclickable');
    }
  });

  $('input[name="data_phone"]').on('input', function () {
    if ($(this).inputmask('isComplete')) {
      $('.s-step__button_validate').removeClass('unclickable');
    } else {
      $('.s-step__button_validate').addClass('unclickable');
    }
  });

  $('#data_post').submit(function (e) {
    e.preventDefault();
    var f = $(this);
    $('.ierror', f).removeClass('ierror');
    var data_type = $('input[name="data_type"]', f).val();
    var data_disk = $('input[name="data_disk"]', f).val();
    var data_category = $('input[name="data_category"]', f).val();
    var data_size = $('input[name="data_size"]', f).val();
    var data_model = $('input[name="data_model"]', f).val();
    var data_person = $('input[name="data_person"]', f).val();
    var data_email = $('input[name="data_email"]', f).val();
    var data_phone = $('input[name="data_phone"]', f).val();
    var data_get = $('input[name="data_get"]', f).val();
    var error = false;
    if (data_type == '') {
      $('input[name="data_type"]', f).addClass('ierror');
      error = true;
    }
    if (data_disk == '') {
      $('input[name="data_disk"]', f).addClass('ierror');
      error = true;
    }
    if (data_category == '') {
      $('input[name="data_category"]', f).addClass('ierror');
      error = true;
    }
    if (data_size == '') {
      $('input[name="data_size"]', f).addClass('ierror');
      error = true;
    }
    if (data_model == '') {
      $('input[name="data_model"]', f).addClass('ierror');
      error = true;
    }
    if (data_person == '') {
      $('input[name="data_person"]', f).addClass('ierror');
      error = true;
    }
    if (data_email == '') {
      $('input[name="data_email"]', f).addClass('ierror');
      error = true;
    }
    if (data_phone == '') {
      $('input[name="data_phone"]', f).addClass('ierror');
      error = true;
    }
    if (data_get == '') {
      $('input[name="data_get"]', f).addClass('ierror');
      error = true;
    }
    if (error) {
      return false;
    }
    $.ajax({
      type: 'POST',
      url: 'mail.php',
      data: $(this).serialize()
    }).done(function (data) {
      if (data.response == 'ok') {
        $('#step_4').slideToggle('fast');
        $('.s-steps').slideToggle('fast');
        window.location.href = 'thanks.html';
        /* $('.s-thanks').slideToggle('fast'); */
      } else {
        alert('Ошибка! Заявка не отправлена, повторите запрос позже');
      }
    });
    return false;
  });

  $('#data_post').submit(function (e) {
    e.preventDefault();
    var f = $(this);
    $('.ierror', f).removeClass('ierror');
    var data_modal_phone = $('input[name="data_modal_phone"]', f).val();
    var error = false;
    if (data_modal_phone == '') {
      $('input[name="data_modal_phone"]', f).addClass('ierror');
      error = true;
    }
    if (error) {
      return false;
    }
    $.ajax({
      type: 'POST',
      url: 'modal.php',
      data: $(this).serialize()
    }).done(function (data) {
      if (data.response == 'ok') {
        $('.s-modal').slideToggle('fast');
        window.location.href = 'thanks.html';
      } else {
        alert('Ошибка! Заявка не отправлена, повторите запрос позже');
      }
    });
    return false;
  });

  $('.s-header__logo').on('click', function () {
    location.reload();
  });
});